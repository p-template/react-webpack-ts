const file: any = {
  fileList: [], // 當前資料夾檔名陣列
  removeFileList: ['./index.js'], // 將要移除的檔案名稱放到陣列
  allFiles: null, // 資料夾中的檔案

  // 取得當前資料夾下所有檔案
  getCurrFolderFiles() {
    this.allFiles = require.context('./', false, /\.tsx$/);
    return this;
  },
  // 移除不想要輸出的檔案
  excludeFiles() {
    if (this.removeFileList.length !== 0) {
      this.fileList = this.allFiles.keys().filter((item: string) => {
        return !this.removeFileList.includes(item);
      });
    }
    return this;
  },
  // 輸出成物件形式
  exportDefaultFiles() {
    let files: {[key: string]: any} = {};

    this.fileList.forEach((path: string) => {
      const viewName = path.replace(/(\.\/|\.tsx)/g, '');
      files[viewName] = this.allFiles(path).default;
    });
    return files;
  },
};

const exportFiles = file
  .getCurrFolderFiles()
  .excludeFiles()
  .exportDefaultFiles();

console.log('exportFiles', exportFiles);

export default exportFiles;
