import { BrowserRouter, Switch } from 'react-router-dom';
import routes from './router';
import '@indigoichigo/scss';
import Lo from '@/layout/layout';
import Nav from '@/layout/Nav';

import '@/styles/index.scss';

function App() {
  return (
    <div className="app">
      <BrowserRouter>
        <Switch>
          <Lo.Layout>
            <Lo.Header>
              <Nav />
            </Lo.Header>
            <Lo.Views>{routes}</Lo.Views>
            <Lo.Footer>footer</Lo.Footer>
          </Lo.Layout>
        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;
