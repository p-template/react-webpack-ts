export interface Iroutes<T> {
  path: string;
  component: T;
  exact: boolean;
  name: string;
}
