import {request} from '@/api';

export const getUserAPI = async () => {
  const res: any = await request({method: 'get', url: '/test/get' });
  return res.data;
};

export const modifyUserAPI = async (data: any) => {
  const res: any = await request({method: 'post', url: '/test/post', data });
  return res.data;
};




