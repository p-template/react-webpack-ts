import * as network from '@indigoichigo/network';
const request = network.restful({ baseURL: 'http://127.0.0.1:8010/', useAuth: false });

export { request };