import styled from 'styled-components';
import { matchPath, useLocation, useHistory } from 'react-router-dom';
import { IFunctionComponent } from '@/interface/IFunctionComponent';
import { Iroutes } from '@/interface/routes';
import { menu as menuRoutes } from '@/router/routes';

const Nav: React.FunctionComponent<IFunctionComponent> = ({ className }) => {
  const location = useLocation();
  const history = useHistory();

  const activedNav: (path: string) => boolean = (path: string) => {
    return matchPath(path, { path: location.pathname })?.isExact ? true : false;
  };

  const toPage = (path: string) => {
    history.push(path);
  };

  const navItem = menuRoutes.map((ele: Iroutes<string>) => (
    <li className="nav-list__item" key={ele.name}>
      <span
        className={`nav-list__item__text ${activedNav(ele.path) ? 'nav-list__item__text--active' : ''}`}
        onClick={() => toPage(ele.path)}
      >
        {ele.name}
      </span>
    </li>
  ));

  return (
    <div className={className}>
      <ul className="nav-list">{navItem}</ul>
    </div>
  );
};

const styledComp = styled(Nav)`
  display: flex;
  justify-content: center;
  width: 100%;
  box-sizing: border-box;
  padding: 15px 0;

  .nav-list {
    display: inline-block;

    &__item {
      display: inline-block;
      vertical-align: middle;
      box-sizing: border-box;
      padding: 0 15px;
      color: #fffafa;
      font-size: 14px;

      &:hover {
        color: #d32f2f;
        transition: 0.4s;
      }

      &__text {
        cursor: pointer;

        &--active {
          position: relative;
          color: #d32f2f;
          font-weight: 700;

          &::before {
            content: '';
            display: inline-block;
            position: absolute;
            bottom: -12px;
            width: 100%;
            height: 4px;
            background-color: #d32f2f;
          }
        }
      }
    }
  }
`;

export default styledComp;
