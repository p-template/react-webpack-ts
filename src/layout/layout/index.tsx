import styled from 'styled-components';
import Header from '@/layout/layout/Header';
import Views from '@/layout/layout/Views';
import Footer from '@/layout/layout/Footer';

const ly: any = {};

ly.Layout = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  height: 100%;
  background-color: #eceff1;
`;

ly.Header = Header;
ly.Views = Views;
ly.Footer = Footer;

export default ly;
