import styled from 'styled-components';

const Header = styled.div`
  flex: 1;
  width: 100%;
  box-sizing: border-box;
  padding: 30px;
`;

export default Header;
