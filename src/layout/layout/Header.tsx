import styled from 'styled-components';

const Header = styled.div`
  flex: none;
  width: 100%;
  height: auto;
  background-color: #e57373;
`;

export default Header;
