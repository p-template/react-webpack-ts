import styled from 'styled-components';

const IFooter = styled.div`
  flex: none;
  width: 100%;
  height: auto;
  box-sizing: border-box;
  padding: 8px;
  color: #333;
  font-size: 12px;
  text-align: center;
`;

const Footer: React.FunctionComponent = () => {
  const currYear: number = new Date().getFullYear();

  return <IFooter>© {currYear}. All rights reserved.</IFooter>;
};

export default Footer;
