import { Iroutes } from '@/interface/routes';

export const basic: Iroutes<string>[] = [
  {
    path: '/',
    component: 'Home',
    exact: true,
    name: '首頁',
  },
];

export const menu: Iroutes<string>[] = [
  {
    path: '/page-one',
    component: 'PageOne',
    name: '頁面一',
    exact: true,
  },
  {
    path: '/page-two',
    component: 'PageTwo',
    name: '頁面二',
    exact: true,
  },
  {
    path: '/page-three',
    component: 'PageThree',
    name: '頁面三',
    exact: true,
  },
 
];
