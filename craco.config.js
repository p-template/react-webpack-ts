const path = require('path');

module.exports = {
  style: {
    sass: {
      loaderOptions: {
        implementation: require('sass'),
        // webpackImporter: false, 
        additionalData: '@import "./src/styles/init.scss";'
      }
    },
  },
  webpack: {
    alias: {
      '@': path.resolve(__dirname, 'src/'),
    },
  },
  devServer: {
    port: 7020,
  },
}